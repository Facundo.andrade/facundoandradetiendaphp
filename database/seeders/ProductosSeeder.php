<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert([
            'nombre' => 'product_name',
            'precio' => 12.5,
            'stock' => 5,
            'descripcion' => 'description product',
            'image' => base64_encode(file_get_contents('/home/itb/Descargas/tux-linux.png')),
        ]);
        DB::table('products')->insert([
            'nombre' => 'product_name2',
            'precio' => 25.5,
            'stock' => 5,
            'descripcion' => 'description product2',
            'image' => base64_encode(file_get_contents('/home/itb/Descargas/discord.png')),
        ]);
        DB::table('products')->insert([
            'nombre' => 'product_name3',
            'precio' => 10.5,
            'stock' => 2,
            'descripcion' => 'description product3',
            'image' => base64_encode(file_get_contents('/home/itb/Descargas/tux-linux.png')),
        ]);
    }
}
