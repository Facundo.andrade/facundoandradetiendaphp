<?php


namespace App\Http\Controllers;

use App\Models\Cart;
use App\Models\Cart_Item;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redirect;


class ProductController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function show(Request $request){
        $valueMin = empty($request->input('priceMin')) ? '0' : $request->input('priceMin');
        $valueMax = empty($request->input('priceMax')) ? '100' : $request->input('priceMax');

        $all = DB::select(DB::raw("select * from products where precio > $valueMin and precio <$valueMax order by 1"));
        $currentPage = LengthAwarePaginator::resolveCurrentPage();
        $itemCollection = collect($all);
        $perPage = 2;
        $currentPageItems = $itemCollection->slice(($currentPage * $perPage) - $perPage, $perPage)->all();
        $products = new LengthAwarePaginator($currentPageItems, count($itemCollection), $perPage);
        $products->setPath($request->url());

        return view('products')->with('products', $products);
    }

    public function emptyCart()
    {
        $user = Auth::user()->id;
        $cart_db = Cart::where("user_id", $user)->first();
        $id = $cart_db->id;
        $cart = [

        ];
        unset($cart[$id]);
        session()->put('cart', $cart);

        DB::select(DB::raw("delete from cart_items where cart_id=$id"));

        return redirect()->back()->with('success', 'Products deleted from cart successfully!');

    }

    public function addToCart($id)
    {

        $product = Product::find($id);

        $user = Auth::user()->id;
        $cart_db = Cart::where("user_id", $user)->first();

        if (empty($cart_db)) {
            $cart_db = new Cart();
            $cart_db->user_id = $user;
            $cart_db->save();
        }

        if (!$product) {
            abort(404);
        }
        $cart = session()->get('cart');

        if (!$cart) {
            $cart = [
                $id => [
                    "nombre" => $product->nombre,
                    "cantidad" => 1,
                    "precio" => $product->precio,
                    "image" => $product->image
                ]
            ];

            $cart_items = new Cart_Item();
            $cart_items->cart_id = $cart_db->id;
            $cart_items->product_id = $id;
            $cart_items->cantidad = 1;
            $cart_items->save();


            session()->put('cart', $cart);
            return redirect()->back()->with('success', 'Product added to cart successfully!');
        }

        if (isset($cart[$id])) {

            $cart[$id]['cantidad']++;
            $cart_item = Cart_Item::where([['cart_id', $cart_db->id], ['product_id', $id]])->first();
            $cart_item->cantidad = $cart[$id]['cantidad'];
            $cart_item->update();
            session()->put('cart', $cart);
            return redirect()->back()->with('success', 'Product added to cart successfully!');
        }

        $cart[$id] = [
            "nombre" => $product->nombre,
            "cantidad" => 1,
            "precio" => $product->precio,
            "image" => $product->image
        ];

        $cart_items = new Cart_Item();
        $cart_items->cart_id = $cart_db->id;
        $cart_items->product_id = $id;
        $cart_items->cantidad = 1;
        $cart_items->cantidad = 1;
        $cart_items->save();

        session()->put('cart', $cart);
        return redirect()->back()->with('success', 'Product added to cart successfully!');
    }

    public function update(Request $request)
    {
        if ($request->id && $request->cantidad) {
            $cart = session()->get('cart');
            $cart[$request->id]["cantidad"] = $request->cantidad;

            $user = Auth::user()->id;
            $cart_db = Cart::where('user_id', $user)->first();
            $cart_item = Cart_Item::where([['cart_id', $cart_db->id], ['product_id', $request->id]])->first();
            $cart_item->cantidad = $request->cantidad;
            $cart_item->update();


            session()->put('cart', $cart);
            session()->flash('success', 'Cart updated successfully');
        }
    }


    public function remove(Request $request)
    {
        if ($request->id) {
            $cart = session()->get('cart');
            if (isset($cart[$request->id])) {
                unset($cart[$request->id]);

                $user = Auth::user()->id;
                $cart_db = Cart::where('user_id', $user)->first();
                $cart_item = Cart_Item::where([['cart_id', $cart_db->id], ['product_id', $request->id]])->first();
                $cart_item->delete();

                session()->put('cart', $cart);
            }
            session()->flash('success', 'Product removed successfully');
        }
    }

    public function venta()
    {
        $user = Auth::user()->id;
        $cart_db = Cart::where("user_id", $user)->first();
        $id = $cart_db->id;

        $products = DB::select(DB::raw("select * from cart_items where cart_id=$id"));
        for ($i=0;$i<count($products);$i++){
            $prodId=$products[$i]->product_id;
            $cantidad=$products[$i]->cantidad;
            $p=Product::find($products[$i]->product_id);
            if ($cantidad > $p->stock){
                return Redirect::back()->with('No hay stcok suficiente');
            }else{
                DB::update(DB::raw("update products set stock =($p->stock-$cantidad) where id=$prodId"));
            }

        }
        DB::delete(DB::raw("delete from cart_items where cart_id=$id"));
        session()->remove('cart');
        return redirect()->back()->with('success', 'Congrats, Compra realizada!! ');

    }

    public function cart()
    {
        return view('cart');
    }

}
