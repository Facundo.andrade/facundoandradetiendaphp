
@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <form method="get" action="/products">
                            <input name="priceMin" placeholder="Precio mínimo" type="text"/>
                            <input name="priceMax" placeholder="Precio máximo" type="text"/>
                            <input value="Busca" type="submit"/>
                            <a href="{{ url('/cart') }}" class="btn btn-warning" style="margin-left: 100px">Cart</a>
                        </form>
                    </div>
                    <div class="title">{{__('Products')}}</div>
                    <div class="card-body">
                        @foreach ($products as $product)

                            <li>{{$product->nombre}}</li>
                            <p class="btn-holder"><a href="{{ url('add-to-cart/'.$product->id) }}" class="btn btn-warning btn-block text-center" role="button">añadir</a> </p>
                            <li>{{$product->precio}}</li>
                            <li>{{$product->stock}}</li>

                            <img src="data:image/jpeg;base64,{!! stream_get_contents($product->image) !!}"/>
                        @endforeach
                    </div>
                    {{ $products->links() }}
                </div> </div> </div> </div>@endsection
