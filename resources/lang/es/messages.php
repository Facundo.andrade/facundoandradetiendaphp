<?php

return [
    'Welcome' => 'Bienvenido',
    'Register' => 'Registrar',
    'Name' => 'Nombre',
    'Surname' => 'Apellido',
    'Password' => 'Contraseña',
    'Confirm Password' => 'Confirmar contraseña',
    'Already registered?' => 'Ya estas registrado?',
];

?>
